<?php

/*
 -------------------------------------------------
 Title: UserInterfaceBLL.php
 Author: Stephen Reed

 Description: This class serves as the class that generates the ui components for the app

 Created: 04/15/2016
 */

$base = basename(getcwd());

if ($base == 'admin') {
    require_once("inc/config.php");
} else {
    require_once("../inc/config.php");
}
//retrieve the db credentials
require_once(dalPath);

class UserInterfaceBLL
{
    protected $_dal;        // the database object
    protected $_imageTypes;    // Image Types Array
    protected $_documentTypes;    // Document Types Array

    public function __construct()
    {
    }

    // setting the private variables from the config file
    protected function init()
    {
        $this->_dal = new dal();
        $this->_imageTypes = unserialize(imageTypes);    // Image Types Array
        $this->_documentTypes = unserialize(documentTypes);    // Document Types Array
    }

    // get the generic information
    protected function getInformation($valuesRequested, $tableNames, $whereClause, $groupByClause, $orderByClause, $limitClause)
    {
        $result = $this->_dal->retrieveFromDB($valuesRequested, $tableNames, $whereClause, $groupByClause, $orderByClause, $limitClause);

        if (!empty($result)) {
            return $result;
        }

    }

    // generate a dropdown based on array
    protected function generateDropDown($index, $array)
    {
        $dropDownAdapter = '';
        foreach ($array as $key => $value) {
            if (!empty($array)) {
                if ($index != $key) {
                    $dropDownAdapter .= '<option value="' . $key . '" >' . $value . '</option>';
                } else {
                    $dropDownAdapter .= '<option value="' . $key . '" selected="selected">' . $value . '</option>';
                }
            }
        }
        return $dropDownAdapter;

    }

    //generate a dropdown based on the Enumerations table
    protected function getDropDown($typeOfDropDown, $index, $sortPreference)
    {
        $dropDownAdapter = '';
        $result = $this->_dal->retrieveFromDB('*', 'Enumerations', 'EnumerationCategory="' . $typeOfDropDown . '"', '', 'EnumerationPriority ' . $sortPreference, '');

        if (!empty($result)) {

            foreach ($result as $value) {
                if ($index != $value['code']) {
                    $dropDownAdapter .= '<option value="' . $value['EnumerationValue'] . '" >' . $value['EnumerationName'] . '</option>';
                } else {
                    $dropDownAdapter .= '<option value="' . $value['EnumerationValue'] . '" selected="selected">' . $value['EnumerationName'] . '</option>';
                }
            }
        }

        return $dropDownAdapter;
    }

    //filter the data based on filter criteria
    protected function checkFilterCriteria($sectionType, $getArray)
    {
        $defaultValues = array('pn' => 1, 'nr' => 13, 'st' => 'ID', 'ob' => 'asc');
        if (empty($_SESSION[$sectionType])) {
            $_SESSION[$sectionType] = array('pn' => 1, 'nr' => 13);
        }
        foreach ($_GET as $criteria => $value) {
            if (array_key_exists($criteria, $defaultValues)) {
                if (!empty($value)) {
                    $_SESSION[$sectionType][$criteria] = $value;
                }
            }
        }
    }

    //generate a generic drop down
    protected function getGeneralDropDown($table, $whereClause, $index, $sortPreference, $optionName, $optionValue)
    {
        $dropDownAdapter = '';

        $result = $this->_dal->retrieveFromDB($optionName . ',' . $optionValue, $table, $whereClause, '', $sortPreference, '');
        if (!empty($result)) {
            foreach ($result as $value) {
                if (!is_array($index)) {
                    $selectedClause = ($value[$optionValue] == $index) ? 'selected = "selected" ' : '';
                } else {
                    $selectedClause = (in_array($value[$optionValue], $index)) ? 'selected = "selected" ' : '';
                }


                $dropDownAdapter .= '<option value="' . $value[$optionValue] . '"' . $selectedClause . '>' . $value[$optionName] . '</option>';
            }
        }
        return $dropDownAdapter;
    }

    //generate a generic checkbox
    protected function getGeneralCheckBox($table, $whereClause, $index, $sortPreference, $optionName, $optionValue, $checkBoxName)
    {
        $checkBoxAdapter = '';
        $result = $this->_dal->retrieveFromDB($optionName . ',' . $optionValue, $table, $whereClause, '', $sortPreference, '');
        if (!empty($result)) {
            foreach ($result as $value) {
                $selectedClause = (@in_array($value[$optionValue], $index)) ? 'checked = "checked" ' : '';


                $checkBoxAdapter .= '<li><input type="checkbox" name="' . $checkBoxName . '[]" value="' . $value[$optionValue] . '" ' . $selectedClause . '> ' . $value[$optionName] . '</li>';
            }
        }
        return $checkBoxAdapter;
    }

    //generate pagination clause
    public function getPaginationClause($fromClause, $whereClause, $pageNum, $numOfResults)
    {
        $pageNum = (!isset($pageNum)) ? 1 : $pageNum;
        $pageNum = ($pageNum < 1) ? 1 : $pageNum;
        $numOfResults = (empty($numOfResults)) ? 5 : $numOfResults;

        $result = $this->_dal->retrieveFromDB('*', $fromClause, $whereClause, '', '', '');

        $numOfRows = sizeof($result);
        $lastPageNum = ceil($numOfRows / $numOfResults);
        $pageNum = ($pageNum > $lastPageNum) ? $lastPageNum : $pageNum;
        //This sets the range to display in our query  where auctionAutho  = "1" and isAuctionLead = "1" ORDER BY   interestContactDate
        $paginationClause = ($pageNum > 0) ? ($pageNum - 1) * $numOfResults . ',' . $numOfResults : '';

        return $paginationClause;
    }

    //generate paginate ui
    protected function getPagination($fromClause, $whereClause, $pageNum, $numOfResults = '', $getPagination)
    {
        $paginationAdapter = '';
        $pageNum = (!isset($pageNum)) ? 1 : $pageNum;
        $pageNum = ($pageNum < 1) ? 1 : $pageNum;
        $numOfResults = (empty($numOfResults)) ? 5 : $numOfResults;

        $result = $this->_dal->retrieveFromDB('*', $fromClause, $whereClause, '', '', '');

        $numOfRows = sizeof($result);
        $lastPageNum = ceil($numOfRows / $numOfResults);
        $pageNum = ($pageNum > $lastPageNum) ? $lastPageNum : $pageNum;
        $prevPageNum = $pageNum - 1;
        $nextPageNum = $pageNum + 1;

        if (!empty($numOfRows)) {
            $paginationAdapter .= '<span class="pagination">';
            $paginationAdapter .= ($pageNum > 1 && $pageNum <= $lastPageNum) ? '<span class="prev"><a href="?pn=' . $prevPageNum . '">&laquo; Prev</a></span>&nbsp;' : '';
            $paginationAdapter .= 'Page<select name="pageno" onChange="document.location.href=\'?pn=\'+this.value+\'&' . $getPagination . '\'">';

            for ($counter = 1; $counter <= $lastPageNum; $counter++) {
                if ($counter == $pageNum) {
                    $paginationAdapter .= '<option value="' . $counter . '" selected="selected">' . $counter . '</option>';

                } else {
                    $paginationAdapter .= '<option value="' . $counter . '">' . $counter . '</option>';
                }
            }

            $paginationAdapter .= '</select> of ' . $lastPageNum;
            $paginationAdapter .= ($pageNum < $lastPageNum) ? '&nbsp;<span class="next"><a href="?pn=' . $nextPageNum . '">Next &raquo;</a></span>' : '';
            $paginationAdapter .= '</span>';
        }

        return $paginationAdapter;

    }

    //generate pagination for blog
    protected function getPaginationBlog($fromClause, $whereClause, $pageNum, $numOfResults = '')
    {
        $paginationAdapter = '';
        $pageNum = (!isset($pageNum)) ? 1 : $pageNum;
        $pageNum = ($pageNum < 1) ? 1 : $pageNum;
        $numOfResults = (empty($numOfResults)) ? 5 : $numOfResults;

        $result = $this->_dal->retrieveFromDB('*', $fromClause, $whereClause, '', '', '');

        $numOfRows = sizeof($result);
        $lastPageNum = ceil($numOfRows / $numOfResults);
        $pageNum = ($pageNum > $lastPageNum) ? $lastPageNum : $pageNum;
        $prevPageNum = $pageNum - 1;
        $nextPageNum = $pageNum + 1;
        $paginationAdapter .= ($pageNum > 1 && $pageNum <= $lastPageNum) ? '<span class="prev"><a href="?pn=' . $prevPageNum . '">&laquo; Prev</a></span>&nbsp;' : '';
        $paginationAdapter .= ($pageNum < $lastPageNum) ? '&nbsp;<span class="next"><a href="?pn=' . $nextPageNum . '">Next &raquo;</a></span>' : '';
        return $paginationAdapter;

    }

    // returns the images from the Images Table based on filter
    protected function getImages($entityType, $entityID, $numOfImages = "")
    {
        $orderByClause = '';
        $groupByClause = '';
        $result = $this->_dal->retrieveFromDB('ImageFileName, ImageTitle, ImageCaption, ImagePriority', 'Images', 'ImageType = "' . $this->_imageTypes[$entityType] . '" and ImageTypeID="' . $entityID . '"', $groupByClause, $orderByClause, $numOfImages);
        $counter = 0;
        foreach ($result as $value) {
            $tempValue[$counter]['ImageFileName'] = $entityID . '_' . $value['ImageFileName'];
            $tempValue[$counter]['ImageTitle'] = $value['ImageTitle'];
            $tempValue[$counter]['ImageCaption'] = $value['ImageCaption'];
            $tempValue[$counter]['ImagePriority'] = $value['ImagePriority'];
            $counter++;
        }
        return $tempValue;
    }

    // returns the document from the Documents Table based on filter
    protected function getDocuments($entityType, $entityID)
    {
        $orderByClause = '';
        $groupByClause = '';
        $result = $this->_dal->retrieveFromDB('DocumentFileName, DocumentCaption,  DocumentPriority', 'Documents', 'DocumentType = "' . $this->_documentTypes[$entityType] . '" and DocumentTypeID="' . $entityID . '"', $groupByClause, $orderByClause, '');
        return $result;
    }

    //formats the phone number based on phone criteria
    protected function formatNumber($number)
    {
        if (is_numeric($number)) {
            $number = "(" . substr($number, 0, 3) . ') ' . substr($number, 3, 3) . '-' . substr($number, 6, 4);
        }
        if (empty($number)) {
            $number = "xxx-xxx-xxxx";
        }
        return $number;
    }

    //format the price based on pricing criteria
    protected function formatPrice($price)
    {
        if (is_numeric($price)) {
            if (strlen($price) > 3) {
                $priceEnd = substr($price, -3, 3);
                $priceRest = substr($price, 0, -3);
                $price = $priceRest . ',' . $priceEnd;
                if (strlen($price) > 7) {
                    $priceEnd = substr($price, -7, 7);
                    $priceRest = substr($price, 0, -7);
                    $price = $priceRest . ',' . $priceEnd;
                }
            }
        }
        if (empty($price)) {
            $price = "0";
        }
        return $price;
    }

    //format address so it is a consistent format
    protected function formatAddress($address, $suite)
    {
        $formattedAddress = !empty($suite) ? $address . " #" . $suite : $address;
        return $formattedAddress;
    }

    //generate success message
    protected function generateSuccessMessage($message)
    {
        return '<div class="fbinfobox">' . $message . '</div>';
    }

    //generate error message
    protected function generateErrorMessage($message)
    {
        return '<div class="fberrorbox">' . $message . '</div>';
    }

    //validate email
    protected function checkEmail($email, $emailTitle)
    {

        if (!filter_input(INPUT_POST, $email, FILTER_VALIDATE_EMAIL)) {
            $statusMessage = $emailTitle . " is invalid, Please re-enter <br />";
        }
        return $statusMessage;
    }

    //check the input of the input field
    protected function checkInput($inputField, $inputTitle)
    {
        $statusMessage = '';
        $statusMessage .= (empty($_POST[$inputField])) ? $inputTitle . " is empty, Please re-enter <br />" : "";
        return $statusMessage;
    }
}

?>