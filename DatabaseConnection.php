<?php

/*
 -------------------------------------------------
 Title: DatabaseConnection.php
 Author: Stephen Reed

 Description: This class serves as the Singleton Design Pattern to only use one instance of the database driver. 

 Created: 01/19/2012
 */
class DatabaseConnection
{
	private function __construct()
	{

		try
		{
			$this->handle = new PDO('mysql:host=' . hostname  . ';dbname=' . dbname  , username, password);
			$this->handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	public static function get()
	{
		static $db = null;
		if($db == null)
		$db = new DatabaseConnection();
		return $db;
	}
	public function handle()
	{
		return $this->handle;
	}
}
?>