<?php
/*
 -------------------------------------------------
 Title: IntakeBLL.php
 Author: Stephen Reed

 Description: This class serves as the Singleton Design Pattern to only use one instance of the database driver.

 Created: 04/15/2016
 */
require_once('UserInterfaceBLL.php');

class IntakeBLL extends UserInterfaceBLL
{
    public function __construct()
    {
        parent::init();
    }

    //strips the <p> tags
    public function stripPTags($value)
    {
        $value = str_replace('<p>', '', $value);
        $value = str_replace('</p>', '', $value);
        return $value;
    }

    // the function get the application and returns an associative array of results
    public function getContent($contentID)
    {

        $psArray = array(':contentID' => $contentID);

        $result = $this->_dal->retrieveFromDBViaPS('*', 'IntakeApplications', 'IAID = :contentID', '', '', 1, $psArray);

        $result[0]["ContactName"] = $result[0]["IAContactName"];
        $result[0]["ContactTitle"] = $result[0]["IAContactTitle"];
        $result[0]["ContactMailingAddress"] = $result[0]["IAContactMailingAddress"];
        $result[0]["ContactCity"] = $result[0]["IAContactCity"];
        $result[0]["ContactState"] = $result[0]["IAContactState"];
        $result[0]["ContactZip"] = $result[0]["IAContactZip"];
        $result[0]["ContactOfficePhone"] = $result[0]["IAContactOfficePhone"];
        $result[0]["ContactCellPhone"] = $result[0]["IAContactCellPhone"];
        $result[0]["ContactEmail"] = $result[0]["IAContactEmail"];
        $result[0]["ContactWebsite"] = $result[0]["IAContactWebsite"];
        $result[0]["ContactPreferredLanguage"] = $result[0]["IAContactPreferredLanguage"];
        $result[0]["ContactPreferredLanguageOther"] = $result[0]["IAContactPreferredLanguageOther"];
        $result[0]["BusinessLocation"] = $result[0]["IABusinessLocation"];
        $result[0]["BusinessLocationOther"] = $result[0]["IABusinessLocationOther"];
        $result[0]["BusinessName"] = $result[0]["IABusinessName"];
        $result[0]["BusinessOwnerName"] = $result[0]["IABusinessOwnerName"];
        $result[0]["BusinessOwnerTitle"] = $result[0]["IABusinessOwnerTitle"];
        $result[0]["BusinessPartnerName"] = $result[0]["IABusinessPartnerName"];
        $result[0]["BusinessPartnerTitle"] = $result[0]["IABusinessPartnerTitle"];
        $result[0]["BusinessAddress"] = $result[0]["IABusinessAddress"];
        $result[0]["BusinessCity"] = $result[0]["IABusinessCity"];
        $result[0]["BusinessState"] = $result[0]["IABusinessState"];
        $result[0]["BusinessZip"] = $result[0]["IABusinessZip"];
        $result[0]["BusinessCount"] = $result[0]["IABusinessCount"];
        $result[0]["BusinessType"] = $result[0]["IABusinessType"];
        $result[0]["BusinessStartDate"] = $result[0]["IABusinessStartDate"];
        $result[0]["BusinessWifi"] = $result[0]["IABusinessWifi"];
        $result[0]["BusinessSellProducts"] = $result[0]["IABusinessSellProducts"];
        $result[0]["BusinessEntityType"] = $result[0]["IABusinessEntityType"];
        $result[0]["BusinessOperationStartTime"] = $result[0]["IABusinessOperationStartTime"];
        $result[0]["BusinessOperationEndTime"] = $result[0]["IABusinessOperationEndTime"];
        $result[0]["BusinessDaysOfOperation"] = $result[0]["IABusinessDaysOfOperation"];
        $result[0]["BusinessWomanOwned"] = $result[0]["IABusinessWomanOwned"];
        $result[0]["BusinessMinorityOwned"] = $result[0]["IABusinessMinorityOwned"];
        $result[0]["BusinessCertifiedWomanOwned"] = $result[0]["IABusinessCertifiedWomanOwned"];
        $result[0]["BusinessCertifiedMinorityOwned"] = $result[0]["IABusinessCertifiedMinorityOwned"];
        $result[0]["BusinessCertifiedDisadvantaged"] = $result[0]["IABusinessCertifiedDisadvantaged"];
        $result[0]["Business8aCertification"] = $result[0]["IABusiness8aCertification"];
        $result[0]["BusinessCertifiedVeteran"] = $result[0]["IABusinessCertifiedVeteran"];
        $result[0]["BusinessSmallBusinessCertification"] = $result[0]["IABusinessSmallBusinessCertification"];
        $result[0]["BusinessNotCertified"] = $result[0]["IABusinessNotCertified"];
        $result[0]["BusinessFullTimeEmployees"] = $result[0]["IABusinessFullTimeEmployees"];
        $result[0]["BusinessPartTimeEmployees"] = $result[0]["IABusinessPartTimeEmployees"];
        $result[0]["BusinessLicense"] = $result[0]["IABusinessLicense"];
        $result[0]["BusinessLicenseCurrent"] = $result[0]["IABusinessLicenseCurrent"];
        $result[0]["BusinessTaxHelp"] = $result[0]["IABusinessTaxHelp"];
        $result[0]["BusinessReceivedLoan"] = $result[0]["IABusinessReceivedLoan"];
        $result[0]["BusinessReferralSource"] = $result[0]["IABusinessReferralSource"];
        $result[0]["DemGender"] = $result[0]["IADemGender"];
        $result[0]["DemRace"] = $result[0]["IADemRace"];
        $result[0]["DemEthnicity"] = $result[0]["IADemEthnicity"];
        $result[0]["DemMilitaryStatus"] = $result[0]["IADemMilitaryStatus"];
        $result[0]["DemVeteranStatus"] = $result[0]["IADemVeteranStatus"];
        $result[0]["DemDisabled"] = $result[0]["IADemDisabled"];
        $result[0]["DemEducation"] = $result[0]["IADemEducation"];
        $result[0]["DemIncome"] = $result[0]["IADemIncome"];
        $result[0]["DemMartialStatus"] = $result[0]["IADemMartialStatus"];
        $result[0]["DemParentsEntreprenuer"] = $result[0]["IADemParentsEntreprenuer"];
        $result[0]["DemHouseholdCount"] = $result[0]["IADemHouseholdCount"];
        $result[0]["DemBirthdate"] = $result[0]["IADemBirthdate"];
        return $result[0];
    }

    //functions that deletes the application based on application id
    public function deleteContent($contentID)
    {
        $psArray = array(':contentID' => $contentID, ':status' => '0');
        $this->_dal->saveToDBViaPS("IntakeApplications", 'IAStatus = :status', 'IAID = :contentID', "", $psArray);
        $result['status'] = 1;
        return $result;
    }

    //function that returns the application based on start date and end date
    public function getContentList($startDate = '', $endDate = '')
    {

        //Filter Results
        if (!empty($startDate) && !empty($endDate)) {
            $unixStartDate = strtotime($startDate);
            $unixEndDate = strtotime($endDate);
            $psArray = array(':IAStatus' => 1, ':startDate' => $unixStartDate, ':endDate' => $unixEndDate);
            $result = $this->_dal->retrieveFromDBViaPS('*', 'IntakeApplications', 'IAStatus = :IAStatus AND UNIX_TIMESTAMP(IATimestamp) BETWEEN (:startDate) AND (:endDate) ', '', '', '', $psArray);
        } else {

            $psArray = array(':IAStatus' => 1);
            $result = $this->_dal->retrieveFromDBViaPS('*', 'IntakeApplications', 'IAStatus = :IAStatus  ', '', '', '', $psArray);
        }


        if (!empty($result)) {
            $infoAdapter = '';
            foreach ($result as $value) {
                $infoAdapter .= ' 
                    <tr>
                         <td>' . $value['IATimestamp'] . '</td>
                        <td>' . $value['IABusinessName'] . '</td>
                        <td>' . $value['IABusinessOwnerName'] . '</td>
                        <td>' . $value['IAContactName'] . '</td>
                        <td><a href="edit.php?contentID=' . $value['IAID'] . '">Edit</a></td>
                        <td><a href="" class="deleteBtn" data-attr="' . $value['IAID'] . '">Delete</a></td>
                    </tr>';

            }
            return $infoAdapter;

        }
    }

    //redirects the user to the main landing page
    public function redirect()
    {
        header('location: ../landing.php');
    }

    //functions that save an applications based on filtered post data.
    public function saveContent($filterArray)
    {
        if (!empty($_SESSION['IntakeID'])) {

            $psArray = array(":ContactName" => $filterArray["ContactName"], ":ContactTitle" => $filterArray["ContactTitle"], ":ContactMailingAddress" => $filterArray["ContactMailingAddress"], ":ContactCity" => $filterArray["ContactCity"], ":ContactState" => $filterArray["ContactState"], ":ContactZip" => $filterArray["ContactZip"], ":ContactOfficePhone" => $filterArray["ContactOfficePhone"], ":ContactCellPhone" => $filterArray["ContactCellPhone"], ":ContactEmail" => $filterArray["ContactEmail"], ":ContactWebsite" => $filterArray["ContactWebsite"], ":ContactPreferredLanguage" => $filterArray["ContactPreferredLanguage"], ":ContactPreferredLanguageOther" => $filterArray["ContactPreferredLanguageOther"], ":BusinessLocation" => $filterArray["BusinessLocation"], ":BusinessLocationOther" => $filterArray["BusinessLocationOther"], ":BusinessName" => $filterArray["BusinessName"], ":BusinessOwnerName" => $filterArray["BusinessOwnerName"], ":BusinessOwnerTitle" => $filterArray["BusinessOwnerTitle"], ":BusinessPartnerName" => $filterArray["BusinessPartnerName"], ":BusinessPartnerTitle" => $filterArray["BusinessPartnerTitle"], ":BusinessAddress" => $filterArray["BusinessAddress"], ":BusinessCity" => $filterArray["BusinessCity"], ":BusinessState" => $filterArray["BusinessState"], ":BusinessZip" => $filterArray["BusinessZip"], ":BusinessCount" => $filterArray["BusinessCount"], ":BusinessType" => $filterArray["BusinessType"], ":BusinessStartDate" => $filterArray["BusinessStartDate"], ":BusinessWifi" => $filterArray["BusinessWifi"], ":BusinessSellProducts" => $filterArray["BusinessSellProducts"], ":BusinessEntityType" => $filterArray["BusinessEntityType"], ":BusinessOperationStartTime" => $filterArray["BusinessOperationStartTime"], ":BusinessOperationEndTime" => $filterArray["BusinessOperationEndTime"], ":BusinessDaysOfOperation" => $filterArray["BusinessDaysOfOperation"], ":BusinessWomanOwned" => $this->formatCheckBoxName($filterArray["BusinessWomanOwned"]), ":BusinessMinorityOwned" => $this->formatCheckBoxName($filterArray["BusinessMinorityOwned"]), ":BusinessCertifiedWomanOwned" => formatCheckBoxName($filterArray["BusinessCertifiedWomanOwned"]), ":BusinessCertifiedMinorityOwned" => formatCheckBoxName($filterArray["BusinessCertifiedMinorityOwned"]), ":BusinessCertifiedDisadvantaged" =>
                $this->formatCheckBoxName($filterArray["BusinessCertifiedDisadvantaged"]), ":Business8aCertification" => $this->formatCheckBoxName($filterArray["Business8aCertification"]), ":BusinessCertifiedVeteran" => $this->formatCheckBoxName($filterArray["BusinessCertifiedVeteran"]), ":BusinessSmallBusinessCertification" => $this->formatCheckBoxName($filterArray["BusinessSmallBusinessCertification"]), ":BusinessNotCertified" => $this->formatCheckBoxName($filterArray["BusinessNotCertified"]), ":BusinessFullTimeEmployees" => $filterArray["BusinessFullTimeEmployees"], ":BusinessPartTimeEmployees" => $filterArray["BusinessPartTimeEmployees"], ":BusinessLicense" => $filterArray["BusinessLicense"], ":BusinessLicenseCurrent" => $filterArray["BusinessLicenseCurrent"], ":BusinessTaxHelp" => $filterArray["BusinessTaxHelp"], ":BusinessReceivedLoan" => $filterArray["BusinessReceivedLoan"], ":BusinessReferralSource" => $filterArray["BusinessReferralSource"], ":DemGender" => $filterArray["DemGender"], ":DemRace" => $filterArray["DemRace"], ":DemEthnicity" => $filterArray["DemEthnicity"], ":DemMilitaryStatus" => $filterArray["DemMilitaryStatus"], ":DemVeteranStatus" => $filterArray["DemVeteranStatus"], ":DemDisabled" => $filterArray["DemDisabled"], ":DemEducation" => $filterArray["DemEducation"], ":DemIncome" => $filterArray["DemIncome"], ":DemMartialStatus" => $filterArray["DemMartialStatus"], ":DemParentsEntreprenuer" => $filterArray["DemParentsEntreprenuer"], ":DemHouseholdCount" => $filterArray["DemHouseholdCount"], ":DemBirthdate" => $filterArray["DemBirthdate"]);

            $this->_dal->saveToDBViaPS('IntakeApplications',
                '   IAContactName = :ContactName,
                    IAContactTitle = :ContactTitle,
                    IAContactMailingAddress = :ContactMailingAddress,
                    IAContactCity = :ContactCity,
                    IAContactState = :ContactState,
                    IAContactZip = :ContactZip,
                    IAContactOfficePhone = :ContactOfficePhone,
                    IAContactCellPhone = :ContactCellPhone,
                    IAContactEmail = :ContactEmail,
                    IAContactWebsite = :ContactWebsite,
                    IAContactPreferredLanguage = :ContactPreferredLanguage,
                    IAContactPreferredLanguageOther = :ContactPreferredLanguageOther,
                    IABusinessLocation = :BusinessLocation,
                    IABusinessLocationOther = :BusinessLocationOther,
                    IABusinessName = :BusinessName,
                    IABusinessOwnerName = :BusinessOwnerName,
                    IABusinessOwnerTitle = :BusinessOwnerTitle,
                    IABusinessPartnerName = :BusinessPartnerName,
                    IABusinessPartnerTitle = :BusinessPartnerTitle,
                    IABusinessAddress = :BusinessAddress,
                    IABusinessCity = :BusinessCity,
                    IABusinessState = :BusinessState,
                    IABusinessZip = :BusinessZip,
                    IABusinessCount = :BusinessCount,
                    IABusinessType = :BusinessType,
                    IABusinessStartDate = :BusinessStartDate,
                    IABusinessWifi = :BusinessWifi,
                    IABusinessSellProducts = :BusinessSellProducts,
                    IABusinessEntityType = :BusinessEntityType,
                    IABusinessOperationStartTime = :BusinessOperationStartTime,
                    IABusinessOperationEndTime = :BusinessOperationEndTime,
                    IABusinessDaysOfOperation = :BusinessDaysOfOperation,
                    IABusinessWomanOwned = :BusinessWomanOwned,
                    IABusinessMinorityOwned = :BusinessMinorityOwned,
                    IABusinessCertifiedWomanOwned = :BusinessCertifiedWomanOwned,
                    IABusinessCertifiedMinorityOwned = :BusinessCertifiedMinorityOwned,
                    IABusinessCertifiedDisadvantaged = :BusinessCertifiedDisadvantaged,
                    IABusiness8aCertification = :Business8aCertification,
                    IABusinessCertifiedVeteran = :BusinessCertifiedVeteran,
                    IABusinessSmallBusinessCertification = :BusinessSmallBusinessCertification,
                    IABusinessNotCertified = :BusinessNotCertified,
                    IABusinessFullTimeEmployees = :BusinessFullTimeEmployees,
                    IABusinessPartTimeEmployees = :BusinessPartTimeEmployees,
                    IABusinessLicense = :BusinessLicense,
                    IABusinessLicenseCurrent = :BusinessLicenseCurrent,
                    IABusinessTaxHelp = :BusinessTaxHelp,
                    IABusinessReceivedLoan = :BusinessReceivedLoan,
                    IABusinessReferralSource = :BusinessReferralSource,
                    IADemGender = :DemGender,
                    IADemRace = :DemRace,
                    IADemEthnicity = :DemEthnicity,
                    IADemMilitaryStatus = :DemMilitaryStatus,
                    IADemVeteranStatus = :DemVeteranStatus,
                    IADemDisabled = :DemDisabled,
                    IADemEducation = :DemEducation,
                    IADemIncome = :DemIncome,
                    IADemMartialStatus = :DemMartialStatus,
                    IADemParentsEntreprenuer = :DemParentsEntreprenuer,
                    IADemHouseholdCount = :DemHouseholdCount,
                    IADemBirthdate = :DemBirthdate'
                , 'IAID = "' . $_SESSION['IntakeID'] . '"', $orderByClause = '', $psArray);


            return parent::generateSuccessMessage(" You have successfully changed your application");
        } else {


            $psArray = array(":ContactName" => $filterArray["ContactName"], ":ContactTitle" => $filterArray["ContactTitle"], ":ContactMailingAddress" => $filterArray["ContactMailingAddress"], ":ContactCity" => $filterArray["ContactCity"], ":ContactState" => $filterArray["ContactState"], ":ContactZip" => $filterArray["ContactZip"], ":ContactOfficePhone" => $filterArray["ContactOfficePhone"], ":ContactCellPhone" => $filterArray["ContactCellPhone"], ":ContactEmail" => $filterArray["ContactEmail"], ":ContactWebsite" => $filterArray["ContactWebsite"], ":ContactPreferredLanguage" => $filterArray["ContactPreferredLanguage"], ":ContactPreferredLanguageOther" => $filterArray["ContactPreferredLanguageOther"], ":BusinessLocation" => $filterArray["BusinessLocation"], ":BusinessLocationOther" => $filterArray["BusinessLocationOther"], ":BusinessName" => $filterArray["BusinessName"], ":BusinessOwnerName" => $filterArray["BusinessOwnerName"], ":BusinessOwnerTitle" => $filterArray["BusinessOwnerTitle"], ":BusinessPartnerName" => $filterArray["BusinessPartnerName"], ":BusinessPartnerTitle" => $filterArray["BusinessPartnerTitle"], ":BusinessAddress" => $filterArray["BusinessAddress"], ":BusinessCity" => $filterArray["BusinessCity"], ":BusinessState" => $filterArray["BusinessState"], ":BusinessZip" => $filterArray["BusinessZip"], ":BusinessCount" => $filterArray["BusinessCount"], ":BusinessType" => $filterArray["BusinessType"], ":BusinessStartDate" => $filterArray["BusinessStartDate"], ":BusinessWifi" => $filterArray["BusinessWifi"], ":BusinessSellProducts" => $filterArray["BusinessSellProducts"], ":BusinessEntityType" => $filterArray["BusinessEntityType"], ":BusinessOperationStartTime" => $filterArray["BusinessOperationStartTime"], ":BusinessOperationEndTime" => $filterArray["BusinessOperationEndTime"], ":BusinessDaysOfOperation" => $filterArray["BusinessDaysOfOperation"], ":BusinessWomanOwned" => $this->formatCheckBoxName($filterArray["BusinessWomanOwned"]), ":BusinessMinorityOwned" => $this->formatCheckBoxName($filterArray["BusinessMinorityOwned"]), ":BusinessCertifiedWomanOwned" => formatCheckBoxName($filterArray["BusinessCertifiedWomanOwned"]), ":BusinessCertifiedMinorityOwned" => formatCheckBoxName($filterArray["BusinessCertifiedMinorityOwned"]), ":BusinessCertifiedDisadvantaged" =>
                $this->formatCheckBoxName($filterArray["BusinessCertifiedDisadvantaged"]), ":Business8aCertification" => $this->formatCheckBoxName($filterArray["Business8aCertification"]), ":BusinessCertifiedVeteran" => $this->formatCheckBoxName($filterArray["BusinessCertifiedVeteran"]), ":BusinessSmallBusinessCertification" => $this->formatCheckBoxName($filterArray["BusinessSmallBusinessCertification"]), ":BusinessNotCertified" => $this->formatCheckBoxName($filterArray["BusinessNotCertified"]), ":BusinessFullTimeEmployees" => $filterArray["BusinessFullTimeEmployees"], ":BusinessPartTimeEmployees" => $filterArray["BusinessPartTimeEmployees"], ":BusinessLicense" => $filterArray["BusinessLicense"], ":BusinessLicenseCurrent" => $filterArray["BusinessLicenseCurrent"], ":BusinessTaxHelp" => $filterArray["BusinessTaxHelp"], ":BusinessReceivedLoan" => $filterArray["BusinessReceivedLoan"], ":BusinessReferralSource" => $filterArray["BusinessReferralSource"], ":DemGender" => $filterArray["DemGender"], ":DemRace" => $filterArray["DemRace"], ":DemEthnicity" => $filterArray["DemEthnicity"], ":DemMilitaryStatus" => $filterArray["DemMilitaryStatus"], ":DemVeteranStatus" => $filterArray["DemVeteranStatus"], ":DemDisabled" => $filterArray["DemDisabled"], ":DemEducation" => $filterArray["DemEducation"], ":DemIncome" => $filterArray["DemIncome"], ":DemMartialStatus" => $filterArray["DemMartialStatus"], ":DemParentsEntreprenuer" => $filterArray["DemParentsEntreprenuer"], ":DemHouseholdCount" => $filterArray["DemHouseholdCount"], ":DemBirthdate" => $filterArray["DemBirthdate"]);

            $newlyInsertedID = $this->_dal->addToDBViaPS('IntakeApplications', 'IAContactName, IAContactTitle, IAContactMailingAddress, IAContactCity, IAContactState, IAContactZip, IAContactOfficePhone, IAContactCellPhone, IAContactEmail, IAContactWebsite, IAContactPreferredLanguage, IAContactPreferredLanguageOther, IABusinessLocation, IABusinessLocationOther, IABusinessName, IABusinessOwnerName, IABusinessOwnerTitle, IABusinessPartnerName, IABusinessPartnerTitle, IABusinessAddress, IABusinessCity, IABusinessState, IABusinessZip, IABusinessCount, IABusinessType, IABusinessStartDate, IABusinessWifi, IABusinessSellProducts, IABusinessEntityType, IABusinessOperationStartTime, IABusinessOperationEndTime, IABusinessDaysOfOperation, IABusinessWomanOwned, IABusinessMinorityOwned, IABusinessCertifiedWomanOwned, IABusinessCertifiedMinorityOwned, IABusinessCertifiedDisadvantaged, IABusiness8aCertification, IABusinessCertifiedVeteran, IABusinessSmallBusinessCertification, IABusinessNotCertified, IABusinessFullTimeEmployees, IABusinessPartTimeEmployees, IABusinessLicense, IABusinessLicenseCurrent, IABusinessTaxHelp, IABusinessReceivedLoan, IABusinessReferralSource, IADemGender, IADemRace, IADemEthnicity, IADemMilitaryStatus, IADemVeteranStatus, IADemDisabled, IADemEducation, IADemIncome, IADemMartialStatus, IADemParentsEntreprenuer, IADemHouseholdCount, IADemBirthdate', ":ContactName, :ContactTitle, :ContactMailingAddress, :ContactCity, :ContactState, :ContactZip, :ContactOfficePhone, :ContactCellPhone, :ContactEmail, :ContactWebsite, :ContactPreferredLanguage, :ContactPreferredLanguageOther, :BusinessLocation, :BusinessLocationOther, :BusinessName, :BusinessOwnerName, :BusinessOwnerTitle, :BusinessPartnerName, :BusinessPartnerTitle, :BusinessAddress, :BusinessCity, :BusinessState, :BusinessZip, :BusinessCount, :BusinessType, :BusinessStartDate, :BusinessWifi, :BusinessSellProducts, :BusinessEntityType, :BusinessOperationStartTime, :BusinessOperationEndTime, :BusinessDaysOfOperation, :BusinessWomanOwned, :BusinessMinorityOwned, :BusinessCertifiedWomanOwned, :BusinessCertifiedMinorityOwned, :BusinessCertifiedDisadvantaged, :Business8aCertification, :BusinessCertifiedVeteran, :BusinessSmallBusinessCertification, :BusinessNotCertified, :BusinessFullTimeEmployees, :BusinessPartTimeEmployees, :BusinessLicense, :BusinessLicenseCurrent, :BusinessTaxHelp, :BusinessReceivedLoan, :BusinessReferralSource, :DemGender, :DemRace, :DemEthnicity, :DemMilitaryStatus, :DemVeteranStatus, :DemDisabled, :DemEducation, :DemIncome, :DemMartialStatus, :DemParentsEntreprenuer, :DemHouseholdCount, :DemBirthdate", $psArray);

            $_SESSION['IntakeID'] = $newlyInsertedID;

            return parent::generateSuccessMessage("You have added keywords successfully ");
        }


    }

    // functions that formats the check box name based on a special condition
    public function formatCheckBoxName($checkBoxName)
    {
        if ($checkBoxName != 'Yes') {
            return 'No';
        }
        return $checkBoxName;

    }
}

?>