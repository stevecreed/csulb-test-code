<?php
/*
 -------------------------------------------------
 Title: StateManagerBLL.php
 Author: Stephen Reed

 Description: This class serves as the class that manages the state of the application

 Created: 04/15/2016
 */

require_once('UserInterfaceBLL.php');
require_once('passwordLib.php');

class StateManagerBLL extends UserInterfaceBLL
{
    //Constructor
    public function __construct()
    {
        parent::init();
    }

    //functions that checks if the user is logged in
    public function isLoggedIn()
    {
        // Check cookie
        if ($_SESSION['DRALoggedIn'] != true) {
            $this->redirectUser("NOT-AUTHORIZED");
        }
    }

    //function that generates landing menu based on the config file.
    public function generateLandingMenu()
    {
        $landingArr = unserialize(landingMenu);
        $landingString = '';
        foreach ($landingArr as $list => $value) {
            $landingString .= '<li><a href="' . $value . '">' . $list . '</a></li>';
        }
        return $landingString;
    }

    //function that generates error messages
    public function generateJSErrorMessage($message)
    {
        return 'alert("' . $message . '");';
    }

    //functions that add admin users to the system
    public function addAdmin()
    {
        $statusMessage = ($this->checkDuplicateEmail($_POST['SignupEmail']) == 1) ? 'Duplicate Email please choose a new one' : '';
        if (empty($statusMessage)) {
            $this->_dal->addToDB("Users", "FirstName, LastName, Email, Password,Status, SignupDate", '"' . $_POST['FirstName'] . '","' . $_POST['LastName'] . '","' . $_POST['SignupEmail'] . '","' . $_POST['Password'] . '","0" , ' . time());
            $_SESSION['UserID'] = $this->_dal->retrieveInsertedID();
            $_SESSION['UserName'] = $_POST['FirstName'] . ' ' . $_POST['LastName'];
            $this->_dal->addToDB("UserPreferences", "UserID", $_SESSION['UserID']);
            header("location: signup1.php");
        } else {
            return $this->generateJSErrorMessage($statusMessage);
        }
    }

    //functions that logs in the User
    public function loginUser($redirectType = '')
    {


        $filterArr = filter_var_array($_POST, FILTER_SANITIZE_STRING);

        $message = '';
        if (empty($filterArr['username'])) {
            $message .= "Username  empty, Please re-enter <br />";
        }
        if (empty($filterArr['password'])) {
            $message .= "Password  empty, Please re-enter <br />";
        }

        if (empty($message)) {
            $psArr = array(':username' => $filterArr['username']);

            $result = $this->_dal->retrieveFromDBViaPS('*', ' Users  ', 'Username = :username ', '', '', '1', $psArr);


            if (sizeof($result) >= 1) {

                if (password_verify($filterArr['password'], $result[0]['UserPassword'])) {
                    $_SESSION['DRALoggedIn'] = true;
                    $_SESSION['UserID'] = $result[0]['UserID'];
                    $this->redirectUser($_SESSION['redirectType'], $result[0]['PrettyUrl']);
                } else {
                    return parent::generateErrorMessage("Im sorry, either your username or password is incorrect, please re-enter");
                }
            } else {
                return parent::generateErrorMessage("Im sorry, either your username or password is incorrect, please re-enter");
            }
        } else {
            return parent::generateErrorMessage($message);
        }
    }

    //function for get users data based on user's data
    public function getUsersWithTracking($dateFrom, $dateTo)
    {

        if (!empty($dateFrom) && !empty($dateTo)) {
            $dateFromUnix = strtotime($dateFrom);
            $dateToUnix = strtotime($dateTo);

            $result = parent::getInformation('*', 'IntakeApplications ', 'UNIX_TIMESTAMP(IATimestamp) BETWEEN ' . $dateFromUnix . ' AND ' . $dateToUnix, '', 'IATimestamp desc', '');
        } else {
            $result = parent::getInformation('*', 'IntakeApplications ', '', '', 'IATimestamp desc', '');
        }

        if (!empty($result)) {
            $data = '';
            foreach ($result as $value) {
                $row1 = array();

                $row1[] = $value["IAContactName"];
                $row1[] = $value["IAContactTitle"];
                $row1[] = $value["IAContactMailingAddress"];
                $row1[] = $value["IAContactCity"];
                $row1[] = $value["IAContactState"];
                $row1[] = $value["IAContactZip"];
                $row1[] = $value["IAContactOfficePhone"];
                $row1[] = $value["IAContactCellPhone"];
                $row1[] = $value["IAContactEmail"];
                $row1[] = $value["IAContactWebsite"];
                $row1[] = $value["IAContactPreferredLanguage"];
                $row1[] = $value["IAContactPreferredLanguageOther"];
                $row1[] = $value["IABusinessLocation"];
                $row1[] = $value["IABusinessLocationOther"];
                $row1[] = $value["IABusinessName"];
                $row1[] = $value["IABusinessOwnerName"];
                $row1[] = $value["IABusinessOwnerTitle"];
                $row1[] = $value["IABusinessPartnerName"];
                $row1[] = $value["IABusinessPartnerTitle"];
                $row1[] = $value["IABusinessAddress"];
                $row1[] = $value["IABusinessCity"];
                $row1[] = $value["IABusinessState"];
                $row1[] = $value["IABusinessZip"];
                $row1[] = $value["IABusinessCount"];
                $row1[] = $value["IABusinessType"];
                $row1[] = $value["IABusinessStartDate"];
                $row1[] = $value["IABusinessWifi"];
                $row1[] = $value["IABusinessSellProducts"];
                $row1[] = $value["IABusinessEntityType"];
                $row1[] = $value["IABusinessOperationStartTime"];
                $row1[] = $value["IABusinessOperationEndTime"];
                $row1[] = $value["IABusinessDaysOfOperation"];
                $row1[] = $value["IABusinessWomanOwned"];
                $row1[] = $value["IABusinessMinorityOwned"];
                $row1[] = $value["IABusinessCertifiedWomanOwned"];
                $row1[] = $value["IABusinessCertifiedMinorityOwned"];
                $row1[] = $value["IABusinessCertifiedDisadvantaged"];
                $row1[] = $value["IABusiness8aCertification"];
                $row1[] = $value["IABusinessCertifiedVeteran"];
                $row1[] = $value["IABusinessSmallBusinessCertification"];
                $row1[] = $value["IABusinessNotCertified"];
                $row1[] = $value["IABusinessFullTimeEmployees"];
                $row1[] = $value["IABusinessPartTimeEmployees"];
                $row1[] = $value["IABusinessLicense"];
                $row1[] = $value["IABusinessLicenseCurrent"];
                $row1[] = $value["IABusinessTaxHelp"];
                $row1[] = $value["IABusinessReceivedLoan"];
                $row1[] = $value["IABusinessReferralSource"];
                $row1[] = $value["IADemGender"];
                $row1[] = $value["IADemRace"];
                $row1[] = $value["IADemEthnicity"];
                $row1[] = $value["IADemMilitaryStatus"];
                $row1[] = $value["IADemVeteranStatus"];
                $row1[] = $value["IADemDisabled"];
                $row1[] = $value["IADemEducation"];
                $row1[] = $value["IADemIncome"];
                $row1[] = $value["IADemMartialStatus"];
                $row1[] = $value["IADemParentsEntreprenuer"];
                $row1[] = $value["IADemHouseholdCount"];
                $row1[] = $value["IADemBirthdate"];

                $data .= join("\t", $row1) . "\n";

            }
            $counter = 0;

        }
        return $data;
    }

    //function for return current age
    public function getCurrentAge($birthDate)
    {
        if (!empty($birthDate)) {
            //explode the date to get month, day and year
            $birthDate = explode("/", $birthDate);
            //get age from date or birthdate
            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
            return $age;
        }
    }

    // function for getting type of activity
    public function getActionType($actionType)
    {
        switch ($actionType) {
            case 1:
                $actionDesc = 'Help a neighbor paint a fence';
                break;
            case 2:
                $actionDesc = ' Volunteer at a local food bank or start a food drive';
                break;
            case 3:
                $actionDesc = 'Start a school or community garden';
                break;
            case 4:
                $actionDesc = 'Update a community park';
                break;
            case 5:
                $actionDesc = 'Help a local organization get technology savvy';
                break;
            case 6:
                $actionDesc = 'Get involved with an invasive plant initiative';
                break;
            case 7:
                $actionDesc = 'Attend a local government meeting';
                break;
            case 8:
                $actionDesc = 'Organize or get involved in a community clean-up day';
                break;
            case 9:
                $actionDesc = 'Create your own project';
                break;

        }
        return $actionDesc;
    }

    //function for redirecting User
    public function redirectUser($redirectType, $specialURL = '')
    {
        switch ($redirectType) {
            case'NOT-AUTHORIZED':
                header('location: ' . path . 'index.php');
                break;
            default:
                header('location: landing.php');
                break;
        }
    }

    //function for get User's Data
    public function getUser()
    {
        $result = $this->_dal->retrieveFromDB('*', ' Users u ', 'UserID = "' . $_SESSION['UserID'] . '"  ', '', '', '');
        return $result[0];
    }

    //function for show showing state
    public function getState($state)
    {
        switch ($state) {
            case 'signout':
                $statusMessage = '<div class="fbinfobox">' . 'You have successfully sign out' . '</div>';
                break;
        }
        return $statusMessage;
    }

    //function for forgot Users
    public function forgotUser()
    {
        $result = $this->_dal->retrieveFromDB('*', ' Users u ', 'Email = "' . $_POST['Email'] . '"  ', '', '', '');
        if (sizeof($result) >= 1) {
            mail("stevecreed@gmail.com", "Password Retrieval from Letterbanc", "Your password is " . $result[0]['Password']);
            return "You password has been successfully sent to your email";
        } else {
            return "Im sorry, you email is not in our system. Please sign up.";
        }
    }

    //function that logs out user
    public function logoutUser()
    {
        session_unset();
        session_destroy();
        $_SESSION = array();

        header("location: index.php?state=signout");
    }

}

?>