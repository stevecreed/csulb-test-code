<?php
/*
 -------------------------------------------------
 Title: Dal.php
 Author: Stephen Reed

 Description: This class serves as the Data Abstraction Layer for Sold 4.0

 Created: 01/19/2012
 */
// Includes the Database Connection
require_once('DatabaseConnection.php');

class Dal
{

    // The database handle for the Class
    private $_databaseHandle;

    //Constructor initialize the database handler
    public function __construct()
    {
        $this->_databaseHandle = databaseConnection::get()->handle();
    }

    //Creating a DB
    public function createToDB($createQuery)
    {
        global $oData;
        try {
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $createQuery . "<br />";
            }
            $this->_databaseHandle->exec($createQuery);
            unset($this->_databaseHandle);
        } catch (PDOException $e) {
            echo "Error Creating to DB... <br /> " . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    }

    // The function performs a insert query with the given paramaters
    public function addToDB($tableName, $columnNameParams, $valueParams)
    {
        global $oData;
        try {
            $insertQuery = 'INSERT INTO ' . $tableName . '(' . $columnNameParams . ') VALUES (' . $valueParams . ')';
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $insertQuery . "<br />";
            }
            $this->_databaseHandle->exec($insertQuery);
            return $this->retrieveInsertedID();
        } catch (PDOException $e) {
            echo "Error Adding to DB... <br /> " . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    } // end add

    public function addToDBViaPS($tableName, $columnNameParams, $valueParams, $arrayParams)
    {
        $arrayParams = $this->checkForNull($arrayParams);
        global $oData;
        try {
            $insertQuery = 'INSERT INTO ' . $tableName . '(' . $columnNameParams . ') VALUES (' . $valueParams . ')';
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $insertQuery . "<br />";
            }
            $ps = $this->_databaseHandle->prepare($insertQuery);
            $ps->execute($arrayParams);

            return $this->retrieveInsertedID();
        } catch (PDOException $e) {
            echo "Error Adding to DB... <br /> " . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    } // end add

    public function addToDBMany($tableName, $columnNameParams, $valueParams)
    {
        global $oData;
        try {
            $insertQuery = 'INSERT INTO ' . $tableName . '(' . $columnNameParams . ') VALUES ' . $valueParams . '';
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $insertQuery . "<br />";
            }
            $this->_databaseHandle->exec($insertQuery);
            return $this->retrieveInsertedID();
        } catch (PDOException $e) {
            echo "Error Adding to DB... <br /> " . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    } // end add

    // This function update a record using the given parameters
    public function saveToDB($tableName, $setClause, $whereClause, $orderByClause)
    {
        global $oData;
        try {
            $updateQuery = ' UPDATE ' . $tableName . ' SET ' . $setClause . ' WHERE ' . $whereClause;
            $updateQuery .= (!empty($orderByClause)) ? ' ORDER BY ' . $orderByClause : "";
            echo $updateQuery . "<br />";
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $updateQuery . "<br />";

                //mail('steve@aesconnect.com', "Query that failed", $updateQuery);
            }
            $this->_databaseHandle->exec($updateQuery);

        } catch (PDOException $e) {
            echo "Error Saving to DB... <br />" . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    }// end save

    public function saveToDBViaPS($tableName, $setClause, $whereClause, $orderByClause, $arrayParams)
    {
        $arrayParams = $this->checkForNull($arrayParams);
        global $oData;
        try {
            $updateQuery = ' UPDATE ' . $tableName . ' SET ' . $setClause . ' WHERE ' . $whereClause;
            $updateQuery .= (!empty($orderByClause)) ? ' ORDER BY ' . $orderByClause : "";
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $updateQuery . "<br />";

                //mail('steve@aesconnect.com', "Query that failed", $updateQuery);
            }
            $ps = $this->_databaseHandle->prepare($updateQuery);
            $ps->execute($arrayParams);

        } catch (PDOException $e) {
            echo "Error Saving to DB... <br />" . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    }// end save

    //This function deletes a record using the given paramters
    public function removeFromDB($tableName, $whereClause)
    {
        global $oData;
        try {
            if (!empty($whereClause)) {
                $deleteQuery = 'DELETE FROM ' . $tableName . ' WHERE ' . $whereClause;
                if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                    echo $deleteQuery . "<br />";
                }
                $this->_databaseHandle->exec($deleteQuery);
            } else {
                throw new Exception('Where Clause cannot be blank');
            }
        } catch (PDOException $e) {
            echo "Error Removing from DB.... <br /> " . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    }// end delete

    //This function deletes a record using the given paramters
    public function removeFromDBViaPS($tableName, $whereClause, $arrayParams)
    {
        $arrayParams = $this->checkForNull($arrayParams);
        global $oData;
        try {
            if (!empty($whereClause)) {
                $deleteQuery = 'DELETE FROM ' . $tableName . ' WHERE ' . $whereClause;
                if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                    echo $deleteQuery . "<br />";
                }

                $ps = $this->_databaseHandle->prepare($deleteQuery);
                $ps->execute($arrayParams);

            } else {
                throw new Exception('Where Clause cannot be blank');
            }
        } catch (PDOException $e) {
            echo "Error Removing from DB.... <br /> " . $e->getMessage();
            echo "<br />Error Occured in <br />" . $e->getTraceAsString();
        }
    }// end delete

    // The function performs a select query using the given parameters
    public function retrieveFromDB($valuesRequested, $tableNames, $whereClause, $groupByClause, $orderByClause, $limitClause)
    {
        global $oData;
        try {
            $selectQuery = 'SELECT ' . $valuesRequested . ' FROM ' . $tableNames;
            $selectQuery .= (!empty($whereClause)) ? ' WHERE ' . $whereClause : "";
            $selectQuery .= (!empty($groupByClause)) ? ' GROUP BY ' . $groupByClause : "";
            $selectQuery .= (!empty($orderByClause)) ? ' ORDER BY ' . $orderByClause : "";
            $selectQuery .= (!empty($limitClause)) ? ' LIMIT ' . $limitClause : "";
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $selectQuery . "<br />";
            }
            $resultFromQuery = $this->_databaseHandle->query($selectQuery);
            return $resultFromQuery->fetchAll(PDO::FETCH_ASSOC);


            //$resultFromQuery = $this->_databaseHandle->query($selectQuery);
            //return $resultFromQuery->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error Retreiving from DB.... <br /> " . $e->getMessage();
            echo "<br />Error Occurred in <br /> " . $e->getTraceAsString();
        }
    } // end retrieve

    public function retrieveFromDBViaPS($valuesRequested, $tableNames, $whereClause, $groupByClause, $orderByClause, $limitClause, $arrayParams)
    {
        $arrayParams = $this->checkForNull($arrayParams);
        global $oData;
        try {
            $selectQuery = 'SELECT ' . $valuesRequested . ' FROM ' . $tableNames;
            $selectQuery .= (!empty($whereClause)) ? ' WHERE ' . $whereClause : "";
            $selectQuery .= (!empty($groupByClause)) ? ' GROUP BY ' . $groupByClause : "";
            $selectQuery .= (!empty($orderByClause)) ? ' ORDER BY ' . $orderByClause : "";
            $selectQuery .= (!empty($limitClause)) ? ' LIMIT ' . $limitClause : "";
            if (debuggingMode == 'on' && in_array($oData['agencyID'], unserialize(adminAgencyIDs))) {
                echo $selectQuery . "<br />";
            }

            $ps = $this->_databaseHandle->prepare($selectQuery);
            $ps->execute($arrayParams);

            return $ps->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            echo "Error Retreiving from DB.... <br /> " . $e->getMessage();
            echo "<br />Error Occurred in <br /> " . $e->getTraceAsString();
        }
    } // end retrieve

    public function retrieveInsertedID()
    {
        return $this->_databaseHandle->lastInsertId();
    }

    private function checkForNull($psArr)
    {

        $psArrWithNoNulls = array_map(function ($param) {
            return ((is_null($param)) ? '' : $param);
        }, $psArr);

        return $psArrWithNoNulls;
    }
}

?>
